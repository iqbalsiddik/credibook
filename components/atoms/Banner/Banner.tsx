import React, { Fragment, useState } from "react";
import { FaPlay } from "react-icons/fa";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { useSelector } from "react-redux";
import StarRatings from "react-star-ratings";
import fetchAxios from "../../../helper/fetchAxios";
import { api_key, env } from "../../../environments/environments";
import Modal from "../Modal/Modal";

export default function Banner() {
  let upcomingMovie = useSelector((state: any) => state.reducerHome.populer);
  const [modal, setModal] = useState(false);
  const [urlTrailer, setUrlTrailer] = useState();

  var slider = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    autoplay: true,
    autoplaySpeed: 5000,
  };

  const showTrailer = async (id: string) => {
    fetchAxios({
      url: env + `/${id}/videos?api_key=${api_key}`,
      method: "get",
    }).then((res) => {
      if (res.data.results.length > 0) {
        setUrlTrailer(res.data.results[0].key);
        setModal(true);
      }
    });
  };

  return (
    <Fragment>
      <Slider {...slider}>
        {upcomingMovie.map((e: any, i: number) => {
          return (
            <div className="h-screen relative" key={i}>
              <div className="w-full h-full relative">
                <div
                  style={{
                    backgroundImage: `url("https://image.tmdb.org/t/p/original/${e.backdrop_path}")`,
                  }}
                  className="w-full h-full bg-cover bg-center"
                />
                <div className="absolute inset-0 w-full h-full bg-main opacity-40"></div>
              </div>
              <div className="flex flex-col justify-center absolute inset-0 z-10 container mx-auto px-10 lg:px-4">
                <h1 className="text-5xl lg:text-7xl font-bold text-white">
                  {e.title}
                </h1>
                <div className="flex items-center space-x-5 py-2 ">
                  <div className="flex items-center space-x-2 py-1">
                    <h1 className="text-gray-200 mt-1 font-semibold">
                      {e.vote_average}
                    </h1>
                    <StarRatings
                      starDimension="20px"
                      rating={(e.vote_average * 5) / 10}
                      starRatedColor="orange"
                      numberOfStars={5}
                      starSpacing="2px"
                      name="rating"
                    />
                  </div>
                </div>
                <p className="text-gray-200 text-lg lg:text-xl lg:max-w-4xl">
                  {e.overview}
                </p>
                <div>
                  <button
                    onClick={() => {
                      showTrailer(e.id);
                    }}
                    className="focus:outline-none px-4 py-2 mt-6 bg-indigo-800 hover:bg-opacity-80 rounded-lg text-white flex items-center justify-center space-x-2 w-32 shadow-lg"
                  >
                    <FaPlay />
                    <h1>Trailer</h1>
                  </button>
                </div>
              </div>
            </div>
          );
        })}
      </Slider>
      <Modal
        show={modal}
        close={() => {
          setModal(false);
        }}
        url={urlTrailer}
      />
    </Fragment>
  );
}
