/* eslint-disable indent */
/* eslint-disable react/jsx-curly-spacing */
/* eslint-disable react/jsx-max-props-per-line */
/* eslint-disable quotes */
import React, { useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";

const NavLink = ({ href, children }: any) => {
  const router = useRouter();
  const isActive = router.pathname === href;
  const className = isActive
    ? "text-white bg-blue-700"
    : "block py-2 pl-3 pr-4 text-gray-700 rounded hover:bg-gray-100 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white";

  return (
    <Link href={href}>
      <div className={`block py-2 pl-3 pr-4 rounded ${className}`}>
        {children}
      </div>
    </Link>
  );
};

export default function Navbar() {
  const [isShowNavbar, setIsShowNavbar] = useState<boolean>(false);

  return (
    <nav className="bg-gray-900 border-gray-200 px-2 sm:px-4 py-2.5 rounded dark:bg-gray-900">
      <div className="container flex flex-wrap items-center justify-between mx-auto">
        <Link href="/home" className="flex items-center">
          <img
            src="https://credibook.com/images/logo-corporate-text.svg"
            className="h-6 mr-3 sm:h-16"
            alt="Flowbite Logo"
          />
          <span className="self-center text-xl font-semibold whitespace-nowrap text-white dark:text-white">
            Movie
          </span>
        </Link>
        <button
          data-collapse-toggle="navbar-hamburger"
          type="button"
          className="inline-flex items-center p-2 ml-3 text-sm text-gray-500 rounded-lg hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
          aria-controls="navbar-hamburger"
          aria-expanded="false"
          onClick={() => setIsShowNavbar(!isShowNavbar)}
        >
          <span className="sr-only">Open main menu</span>
          <svg
            className="w-6 h-6"
            aria-hidden="true"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fill-rule="evenodd"
              d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
              clip-rule="evenodd"
            ></path>
          </svg>
        </button>
        <div
          className={`${isShowNavbar ? "block" : "hidden"} w-full`}
          id="navbar-hamburger"
        >
          <ul className="flex flex-col mt-4 rounded-lg bg-gray-800 dark:bg-gray-800 dark:border-gray-700">
            <li>
              <NavLink href="/home">Home</NavLink>
            </li>
            <li>
              <NavLink href="/about">About</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}
