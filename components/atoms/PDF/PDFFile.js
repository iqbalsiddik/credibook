import React from "react";
import { Page, Text, Image, Document, StyleSheet } from "@react-pdf/renderer";

export default function PDFFile(data) {
  let items = data.data;

  const styles = StyleSheet.create({
    body: {
      paddingTop: 35,
      paddingBottom: 65,
      paddingHorizontal: 35,
    },
    title: {
      fontSize: 24,
      textAlign: "center",
    },
    text: {
      marginTop: 30,
      margin: 12,
      fontSize: 14,
      textAlign: "justify",
      fontFamily: "Times-Roman",
    },
    image: {
      marginVertical: 15,
      marginHorizontal: 100,
    },
    header: {
      fontSize: 12,
      marginBottom: 20,
      textAlign: "center",
      color: "grey",
    },
    pageNumber: {
      position: "absolute",
      fontSize: 12,
      bottom: 30,
      left: 0,
      right: 0,
      textAlign: "center",
      color: "grey",
    },
  });

  return (
    <Document>
      <Page style={styles.body}>
        <Text style={styles.header} fixed>
          {items.title}
        </Text>
        <Image
          style={styles.image}
          src={`https://image.tmdb.org/t/p/original/${items.poster_path}`}
        />
        <Text style={styles.text} fixed>
          {items.overview}
        </Text>
      </Page>
    </Document>
  );
}
