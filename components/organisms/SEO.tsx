import { NextSeo } from "next-seo";
import React from "react";

interface ISEO {
  title?: string;
  description?: string;
}
const SEO: React.FC<ISEO> = (props) => {
  const {
    title = "Cardibook : Commerce platform for wholesale shopping in Indonesia",
    description = "Commerce platform for wholesale shopping in Indonesia",
  } = props;
  return <NextSeo title={title} description={description} />;
};
export default SEO;
