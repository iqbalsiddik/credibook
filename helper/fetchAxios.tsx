import Axios from "axios";
import { toast } from "react-toastify";
import { TFetchAxios } from "../interfaces/common";

const fetchAxios = async (props: TFetchAxios) => {
  const { url, method, body, token } = props;

  if (token) {
    Axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  }

  try {
    const response: any = await Axios({ method: method, url: url, data: body });
    if (response.status <= 299) {
      return Promise.resolve(response);
    } else if (response.status === 400) {
      toast.error(response.message);
      window.location.href = "/404";
      return Promise.resolve(response);
    } else if (response.status === 500) {
      toast.error(response.message);
    } else if (response.status === 404) {
      toast.error(response.message);
    } else if (response.status === 401) {
      toast.error(response.message);
    } else {
      toast.error(response.message);
    }
  } catch (err: any) {
    const error = err.toJSON();
    if (error.status === 422) {
      toast.error(err.response?.data?.message);
      return Promise.resolve(error);
    } else if (error.status === 401) {
      toast.error(error.message);
      return Promise.resolve(error);
    } else if (error.status === 500) {
      toast.error(error.message);
    }
    return Promise.resolve(error);
  }
};

export default fetchAxios;
