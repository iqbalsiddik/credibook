import "@/styles/globals.css";
import "react-toastify/dist/ReactToastify.css";
import type { AppProps } from "next/app";
import Head from "next/head";
import { Fragment, Suspense } from "react";
import { Provider } from "react-redux";
import { ToastContainer } from "react-toastify";
import store from "store/store";
import Loading from "components/atoms/Loading/loading";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <Fragment>
      <Suspense fallback={<Loading />}>
        <Provider store={store}>
          <Head>
            <link
              rel="icon"
              sizes="32x32"
              type="image/png"
              href="/public/credibook.webp"
            />
          </Head>
          <ToastContainer />
          <Component {...pageProps} />
        </Provider>
      </Suspense>
    </Fragment>
  );
}
