import Layout from "components/atoms/Layout/layout";
import React from "react";

export default function About() {
  return (
    <Layout title="About" desc="Tentang CridiBook movie">
      <div className="overflow-x-hidden">
        <div className="min-h-screen min-w-full flex items-center">
          <div>
            <div className="grid grid-cols-6 gap-4  text-center">
              <div className="col-start-2 col-span-4 ">
                <p className="text-white font-bold text-4xl">About</p>
              </div>
            </div>
            <div className="grid grid-cols-6 gap-4 text-center my-10">
              <div className="col-start-2 col-span-4 ">
                <p className="text-white text-xl tracking-wide">
                  Credibox adalah layanan streaming yang memungkinkan siapapun
                  dapat menonton acara TV dan film di perangkat yang terhubung
                  ke internet. Film dan series bervariasi menurut wilayah dan
                  dapat berubah seiring waktu. Anda dapat menonton berbagai
                  film-film keluaran terbaru, membaca sinopsis film tersebut
                  sebelum ditonton, melihat trailer film yang akan datang, dll.
                </p>
                <br />
                <p className="text-white text-xl tracking-wide">
                  Semakin banyak Anda menonton, semakin baik Credibox
                  merekomendasikan film.
                </p>
                <br />
                <p className="text-white text-xl tracking-wide">
                  Anda dapat menonton dari Credibox melalui perangkat apa pun
                  yang tersambung ke internet. Anda juga dapat menonton Credibox
                  di komputer menggunakan browser internet. Anda dapat meninjau
                  persyaratan sistem untuk kompatibilitas browser web, dan
                  memeriksa rekomendasi kecepatan internet kami untuk mencapai
                  performa terbaik.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}
