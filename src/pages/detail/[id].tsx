import React, { useEffect, useState } from "react";
import StarRatings from "react-star-ratings";
import InfiniteScroll from "react-infinite-scroller";
import { PDFDownloadLink } from "@react-pdf/renderer";
import { FaPlay, FaCloudDownloadAlt } from "react-icons/fa";
import { useRouter } from "next/router";

import fetchAxios from "helper/fetchAxios";
import { api_key, env } from "environments/environments";
import PDFFile from "../../../components/atoms/PDF/PDFFile";
import Layout from "components/atoms/Layout/layout";
import CardMovie from "components/atoms/Card/CardMovie";
import Skeleton from "components/atoms/Skeleton/skeleton";
import Modal from "components/atoms/Modal/Modal";
import { IMovie } from "interfaces/common";

export default function DetailHome() {
  let { query, isReady } = useRouter();
  const [modal, setModal] = useState(false);
  const [urlTrailer, setUrlTrailer] = useState();
  const [movie, setMovie] = useState<IMovie>({
    id: 0,
    title: "",
    vote_average: 0,
    backdrop_path: "",
    overview: "",
  });
  const [movieSimilar, setMovieSimilar] = useState({
    data: [],
    page: 1,
  });

  useEffect(() => {
    if (isReady) {
      fetchMovie();
    }
  }, [isReady, query]);

  useEffect(() => {
    if (isReady) {
      getMovieSimilar(1);
    }
  }, [isReady]);

  const fetchMovie = () => {
    fetchAxios({
      url: env + `/${query.id}?api_key=${api_key}`,
      method: "get",
    }).then((res) => {
      setMovie(res.data);
    });
  };

  const showTrailer = async (id: number) => {
    fetchAxios({
      url: env + `/${id}/videos?api_key=${api_key}`,
      method: "get",
    }).then((res) => {
      if (res.data.results.length > 0) {
        setUrlTrailer(res.data.results[0].key);
        setModal(true);
      }
    });
  };

  const getMovieSimilar = async (page?: number) => {
    fetchAxios({
      url: env + `/${query.id}/similar?api_key=${api_key}&page=${page}`,
      method: "get",
    }).then((res) => {
      if (page === 1) {
        setMovieSimilar({
          page: res.data.page,
          data: res.data.results.filter((e: any) => e.poster_path !== null),
        });
        window.scrollTo(0, 0);
      } else {
        setMovieSimilar({
          page: res.data?.page,
          data: movieSimilar.data.concat(
            res.data?.results.filter((e: any) => e.poster_path !== null)
          ),
        });
      }
    });
  };

  return (
    <Layout title="Detail Movie" desc="Detail tentang movie">
      <div className="overflow-x-hidden">
        <div className="h-screen relative">
          <div className="w-full h-full relative">
            <div
              style={{
                backgroundImage: `url("https://image.tmdb.org/t/p/original/${movie.backdrop_path}")`,
              }}
              className="w-full h-full bg-cover bg-center"
            />
            <div className="absolute inset-0 w-full h-full bg-main opacity-40"></div>
          </div>
          <div className="flex flex-col justify-center absolute inset-0 z-10 container mx-auto px-8 lg:px-4">
            <h1 className="text-5xl lg:text-7xl font-bold text-white">
              {movie.title}
            </h1>
            <div className="flex items-center space-x-5 py-2">
              <div className="flex items-center space-x-2 py-1">
                <h1 className="text-gray-300 mt-1 font-semibold">
                  {movie.vote_average}
                </h1>
                <StarRatings
                  starDimension="20px"
                  rating={
                    ((movie.vote_average ? movie.vote_average : 0) * 5) / 10
                  }
                  starRatedColor="orange"
                  numberOfStars={5}
                  starSpacing="2px"
                  name="rating"
                />
              </div>
            </div>
            <p className="text-gray-200 text-lg lg:text-xl lg:max-w-4xl">
              {movie.overview}
            </p>
            <div>
              <button
                onClick={() => {
                  showTrailer(movie?.id);
                }}
                className="focus:outline-none px-4 py-2 mt-6 bg-indigo-800 hover:bg-opacity-80 rounded-lg text-white flex items-center justify-center space-x-2 w-32 shadow-lg"
              >
                <FaPlay />
                <h1>Trailer</h1>
              </button>
            </div>
            <div>
              {isReady ? (
                <PDFDownloadLink document={<PDFFile data={movie} />}>
                  {({ loading }) =>
                    loading ? (
                      <button className="focus:outline-none px-4 py-2 mt-6 bg-indigo-800 hover:bg-opacity-80 rounded-lg text-white flex items-center justify-center space-x-2 w-42 shadow-lg">
                        <FaCloudDownloadAlt />
                        <h1>Loading ...</h1>
                      </button>
                    ) : (
                      <button className="focus:outline-none px-4 py-2 mt-6 bg-indigo-800 hover:bg-opacity-80 rounded-lg text-white flex items-center justify-center space-x-2 w-42 shadow-lg">
                        <FaCloudDownloadAlt />
                        <h1>Download Info Movie </h1>
                      </button>
                    )
                  }
                </PDFDownloadLink>
              ) : null}
            </div>
          </div>
        </div>
        {movieSimilar.data.length > 0 ? (
          <div className="min-h-screen bg-main -mt-2">
            <div className="container mx-auto py-24 px-8 lg:px-4">
              <h1 className="text-3xl text-gray-300 font-semibold">
                Similar Movies
              </h1>
              <InfiniteScroll
                pageStart={1}
                loadMore={(e) => getMovieSimilar(e)}
                hasMore={true}
                loader={
                  <div className="text-white" key={1}>
                    Loading ...
                  </div>
                }
                className="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 mt-8 gap-x-5 gap-y-10"
              >
                {movieSimilar.data.map((e, i) => {
                  return <CardMovie data={e} key={i} />;
                })}
              </InfiniteScroll>
            </div>
          </div>
        ) : (
          <div className="h-screen my-11 flex flex-wrap gap-2">
            {Array.from({ length: 8 }, (_, i: number) => (
              <Skeleton key={i} />
            ))}
          </div>
        )}
        <Modal
          show={modal}
          close={() => {
            setModal(false);
          }}
          url={urlTrailer}
        />
      </div>
    </Layout>
  );
}
