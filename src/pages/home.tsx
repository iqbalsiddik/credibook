import type { NextPage } from "next";
import { useDispatch } from "react-redux";
import InfiniteScroll from "react-infinite-scroller";
import { Fragment, useCallback, useEffect, useState } from "react";

import fetchAxios from "helper/fetchAxios";
import { actionUpComing } from "store/actions";
import Banner from "components/atoms/Banner/Banner";
import Layout from "components/atoms/Layout/layout";
import CardMovie from "components/atoms/Card/CardMovie";
import Skeleton from "components/atoms/Skeleton/skeleton";
import environments, { api_key, env } from "environments/environments";

const Home: NextPage = () => {
  const dispatch = useDispatch();
  const [popularMovie, setPopularMovie] = useState({
    data: [],
    page: 1,
  });

  useEffect(() => {
    fetchUpComingMovie();
  }, []);

  useEffect(() => {
    (async function () {
      await getMovie(1);
    })();
  }, []);

  const fetchUpComingMovie = useCallback(() => {
    fetchAxios({
      url:
        environments.api.popular +
        `?api_key=${api_key}&page=${Math.floor(
          Math.random() * (400 - 1 + 1) + 1
        )}`,
      method: "get",
    }).then((response) => {
      if (response.status === 200) {
        dispatch(
          actionUpComing(
            response.data?.results?.filter((e: any) => e.backdrop_path !== null)
          )
        );
      }
    });
  }, []);

  const getMovie = (page?: number) => {
    fetchAxios({
      url: env + `/now_playing?api_key=${api_key}&page=${page}`,
      method: "get",
    }).then((res) => {
      if (res.status === 200) {
        setPopularMovie({
          page: res.data?.page,
          data: popularMovie?.data?.concat(
            res.data?.results.filter((e: any) => e.poster_path !== null)
          ),
        });
      }
    });
  };

  return (
    <Fragment>
      <Layout title="Home Page" desc="List movie terpopuler tahun 2022">
        <div className="overflow-x-hidden">
          <Banner />
          {popularMovie.data.length > 0 ? (
            <div className="min-h-screen bg-main -mt-2">
              <div className="container mx-auto py-24 px-8 lg:px-4">
                <h1 className="text-3xl text-gray-300 font-semibold">
                  Now Playing
                </h1>
                <InfiniteScroll
                  pageStart={1}
                  loadMore={(e) => getMovie(e)}
                  hasMore={true}
                  loader={
                    <div className="loader" key={1}>
                      Loading ...
                    </div>
                  }
                  className="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 mt-8 gap-x-5 gap-y-10"
                >
                  {popularMovie.data.map((e, i) => {
                    return <CardMovie data={e} key={i} />;
                  })}
                </InfiniteScroll>
              </div>
            </div>
          ) : (
            <div className="h-screen my-11 flex flex-wrap gap-2">
              {Array.from({ length: 8 }, (_, i: number) => (
                <Skeleton key={i} />
              ))}
            </div>
          )}
        </div>
      </Layout>
    </Fragment>
  );
};

export default Home;
