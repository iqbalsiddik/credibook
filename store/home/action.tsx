import { PDFDATA, POPULERMOVIE } from "./actionTypes";

export const actionUpComing = (data: any) => {
  return {
    type: POPULERMOVIE,
    payload: data,
  };
};

export const actionDataPDF = (data: any) => {
  return {
    type: PDFDATA,
    payload: data,
  };
};
