import { PDFDATA, POPULERMOVIE } from "./actionTypes";

type IReduceHome = {
  loading: boolean;
  populer: object[];
  downloadPDF: object[];
};

type IAction = {
  type: string;
  payload?: any;
};

const initialState: IReduceHome = {
  loading: true,
  populer: [],
  downloadPDF: [],
};

const reducerHome = (state: IReduceHome = initialState, action: IAction) => {
  switch (action.type) {
    case POPULERMOVIE:
      state = {
        ...state,
        populer: action.payload,
      };
      break;
    case PDFDATA:
      state = {
        ...state,
        downloadPDF: action.payload,
      };
      break;
  }
  return state;
};

export default reducerHome;
