import { combineReducers } from "redux";
import reducerHome from "./home/reducer";

const rootReducer = combineReducers({
  reducerHome: reducerHome,
});
export type AppState = ReturnType<typeof rootReducer>;
export default rootReducer;
